package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty() || taskId == null || taskId.isEmpty()) return null;
        return taskRepository.bindTaskByProject(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskByProjectId(taskId);
    }

    @Override
    public List<Task> removeTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeProjectById(final String projectId){
        if (projectId == null || projectId.isEmpty()) return null;
        if(removeTasksByProjectId(projectId) == null) return projectRepository.removeOneById(projectId);
        return null;
    }

}
