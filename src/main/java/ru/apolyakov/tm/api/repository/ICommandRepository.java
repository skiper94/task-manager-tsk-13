package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}

