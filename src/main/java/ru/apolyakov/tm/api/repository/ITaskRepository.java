package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAllTaskByProjectId(String projectId);

    List<Task> removeAllTaskByProjectId(String projectId);

    Task bindTaskByProject(String projectId, String taskId);

    Task unbindTaskByProjectId(String taskId);

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);


}
